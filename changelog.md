﻿## Changelog
* **BETA4-3 is out** (30/05/2014 13:00 GMT+1)
	* Updated memory addresses FUTWC patch, **old versions don't work anymore**.
* **BETA4-2 is out** (20/05/2014 18:30 GMT+1) **Video**: https://www.youtube.com/watch?v=PtWC37YSciM
	* removed "--old-name-pointer" shortcuts, the command line switch is still there even if no one should use it (no reported problems so far).
	* launching the browser automatically doesn't steal the focus from the active window, launching it manually will activate the browser's window.
	* new feature: opponent's match history is now opened alongside the profile. (Thank you Reinaldo Filho Camargo for the idea!)
	* included "bounce.html" and "frames.html", both used to frame opponent's match history and profile and nothing else, check their source.
	* minor fairness improvements, all the warnings are still in place.
* **BETA4-1 is out** (15/05/2014 17:00 GMT+1)
	* fixed that for someone CheatMeNot14 wasn't launching or it was freezing after every opponent, it's now reading the opponent name from a more realiable memory location.
	* added "--old-name-pointer" switch to use the old memory location, use this only if CheatMeNot14 was working fine for you and **only** if now it doesn't anymore (and please report me the issue).
	* included two .bat files to launch CheatMeNot with the "--old-name-pointer" switch (again, don't use this unless you're having problems that you never had before).
	* the tool doesn't need to be restarted anymore if you quit and re-enter FUT.
	* the tool doesnt't shut down anymore if you quit and re-enter FUT.
	* changed the base URL for profiles, now using the international mirror instead of the uk one. (Log in @ http://www.easports.com/fifa/football-club if the stats are unavailable.)
	* launching the browser will not steal the focus from the active window anymore.
	* the established date is now saved in the blacklist for statistical purposes since most cheaters use new accounts (old lists are still compatible).
	* reformatted the output to save some space vertically.
	* sorted out few typos.
	* fixed that LB and RB positions in 451(2) were inverted.
* **BETA5 in development**, it will feature various black/whitelist improvements
    * multiple blacklists and whitlelists support, you can add other people's lists to yours (if you trust them).
	* the tool will tell you on which list an opponent is, eg. "whitelisted on XXX's list", "blacklisted on ZZZ's list", etc.
	* blacklisting someone will remove that someone from the whitelist and vice versa.
* Reuploaded BETA4, if you have missing dlls issues install the [VS2013 redistributable package](http://www.microsoft.com/en-us/download/details.aspx?id=40784).
* **BETA4 is out** (12/05/2014 19:00 GMT+1) **Video**: http://youtu.be/PtWC37YSciM
	* added "--auto-open-profile" switch to automatically launch your browser at your opponent's profile.
	* added "ALT+c" hotkey to manually launch your browser at your opponent's profile.
	* included a .bat file to launch CheatMeNot with the "--auto-open-profile" switch.
	* profile URL doesn't get copied in clipboard anymore.
* **BETA3 is out** (11/05/2014 20:00 GMT+1)
    * fixed again that IFs, TOTS, MOTM etc, cards were not counted.
	* **new feature**, opponent's profile url gets copied in your clipboard automatically, paste it in your browser to check his matches and winrate.
	![profile of a cheater](http://i.imgur.com/4y4K9dE.jpg)
	* fixed that RM and CDM positions in 442(1) were inverted.
* **BETA2 is out** (09/05/2014 04:30 GMT+1)
    * fixed that IFs, TOTS, MOTM etc, cards were not counted.
	* fixed various minor formation-checks errors.
	* blacklist and whitelist can now record multiple reports for the same user.
	* blacklist and whitelist now also save the club name (old lists are still compatible).
* **BETA1 is out** (08/05/2014 03:00 GMT+1)